package br.com.fifo.entity;

import java.util.LinkedList;
import java.util.Queue;

public class PersonQueue {

    private Queue<Person> queue;

    public PersonQueue() {
        this.queue = new LinkedList<>();
    }

    public String insertPerson(Person person){
        if (queue.isEmpty() || !person.isElderly()){
            queue.add(person);
        }else{
            Queue<Person> newQueue = new LinkedList<>();
            while (!queue.isEmpty()){
                Person newPerson = queue.remove();
                if (newPerson.isElderly() || person == null){
                    newQueue.add(newPerson);
                }else{
                    newQueue.add(person);
                    newQueue.add(newPerson);
                    person = null;
                }
            }
            queue = newQueue;
        }
        return queue.toString();
    }

    public String attendPerson(){
        if (queue.isEmpty()){
            return null;
        }
        queue.remove();
        return queue.toString();
    }
}
