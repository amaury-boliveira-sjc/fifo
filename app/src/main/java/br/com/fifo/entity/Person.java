package br.com.fifo.entity;

public class Person {

    private String name;

    private boolean elderly;

    public Person(String name, boolean elderly) {
        this.name = name;
        this.elderly = elderly;
    }

    public boolean isElderly() {
        return elderly;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", elderly=" + elderly +
                '}';
    }

    public String validate(){
        if (name == null || name.isEmpty()){
            return "Campo vazio, por favor insira um nome";
        }
        return null;
    }
}
