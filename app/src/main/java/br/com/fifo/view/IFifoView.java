package br.com.fifo.view;

public interface IFifoView {

    void showFifo(String fila);

    void showError(String error);
}
