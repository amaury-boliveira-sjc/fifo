package br.com.fifo.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import br.com.fifo.R;
import br.com.fifo.presenter.FifoPresenter;

public class FifoActivity extends AppCompatActivity implements IFifoView, Button.OnClickListener{

    private EditText editText;
    private CheckBox checkBox;
    private Button buttonInsert;
    private Button buttonAttend;
    private TextView textView;
    private FifoPresenter fifoPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fifo_activity);

        initComponents();
        initListeners();
    }

    private void initComponents(){
        this.editText = findViewById(R.id.edit_text_names);
        this.checkBox = findViewById(R.id.check_box_yes);
        this.buttonInsert = findViewById(R.id.button_insert);
        this.buttonAttend = findViewById(R.id.button_attend);
        this.textView = findViewById(R.id.text_view_fifo_names);
        this.fifoPresenter = new FifoPresenter(this);
    }

    private void initListeners(){
        this.buttonInsert.setOnClickListener(this);
        this.buttonAttend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_insert) {
            if (checkBox.isChecked()) {
                fifoPresenter.insertPerson(editText.getText().toString(), true);
            } else {
                fifoPresenter.insertPerson(editText.getText().toString(), false);
            }
        }else{
            fifoPresenter.attendPerson();
        }
    }

    @Override
    public void showFifo(String queue) {
        this.textView.setText(queue);
        editText.getText().clear();
        checkBox.setChecked(false);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }
}
