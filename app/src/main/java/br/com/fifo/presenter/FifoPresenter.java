package br.com.fifo.presenter;

import br.com.fifo.entity.Person;
import br.com.fifo.entity.PersonQueue;
import br.com.fifo.view.IFifoView;

public class FifoPresenter {

    private IFifoView fifoView;
    private PersonQueue personQueue;

    public FifoPresenter(IFifoView fifoView) {
        this.fifoView = fifoView;
        this.personQueue = new PersonQueue();
    }

    public void insertPerson(String name, boolean elderly){
        Person person = new Person(name, elderly);
        String error = person.validate();
        if (error == null){
            String queue = personQueue.insertPerson(person);
            fifoView.showFifo(queue);
        }else{
            fifoView.showError(error);
        }
    }

    public void attendPerson(){
        String queue = personQueue.attendPerson();
        if (queue == null){
            fifoView.showError("Todas as pessoas na fila já foram atendidas");
        }else{
            fifoView.showFifo(queue);
        }
    }
}
